omronModules.factory('ExpenseType', [
    '$resource',
    'TokenHandler',
    'Config',
function($resource, TokenHandler, Config) {

    var resource = $resource(Config.base + '/expensesTypes', {}, {
        'query' : { method : 'GET', isArray:false }
    });

    resource = TokenHandler.wrapActions( resource, ['get'] );

    return resource;

}])