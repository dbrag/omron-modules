omronModules.factory('Exam', [
    '$resource',
    'TokenHandler',
    'Config',
    function($resource, TokenHandler, Config)
    {
        var resource = $resource(Config.base + '/exams/:id', {id: '@id'}, {
            'query' : {method : 'GET', isArray:false},
            'update' : { method : 'PUT' }
        });

        resource = TokenHandler.wrapActions(resource, ['get', 'save', 'update', 'query']);

        return resource;
    }
]);