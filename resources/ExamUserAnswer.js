omronModules.factory('ExamUserAnswer', [
    '$resource',
    'TokenHandler',
    'Config',
    function($resource, TokenHandler, Config)
    {
        var resource = $resource(Config.base + '/exam/user/answers', {id: '@id'}, {
            'query': {method : 'GET', isArray:false},
        });

        resource = TokenHandler.wrapActions(resource, ['save']);
        return resource;
    }
]);