omronModules.factory('UserJob', [
    '$resource',
    'TokenHandler',
    'Config',
function($resource, TokenHandler, Config) {

    var resource = $resource(Config.base + '/jobs/:id', { id: '@id' }, {
        'update' : { method : 'PUT' },
        'query' : { method : 'GET', isArray:false }
    });

    resource = TokenHandler.wrapActions( resource, ['get', 'query'] );

    return resource;

}])