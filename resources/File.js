omronModules.factory('File', [
    '$resource',
    'TokenHandler',
    'Config',
function($resource, TokenHandler, Config) {

    var resource = $resource(Config.base + '/files/:id', { id: '@id' }, {
        'update' : { method : 'PUT' },
        'query' : { method : 'GET', isArray : false },
        'download' : { method : 'GET', isArray : false , url: Config.base + '/files/:id/download' },
    });

    resource = TokenHandler.wrapActions( resource, ['get', 'save', 'query', 'update' , 'download'] );

    return resource;

}])