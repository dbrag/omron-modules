omronModules.factory('Scheduling', [
    '$resource',
    'TokenHandler',
    'Config',
function($resource, TokenHandler, Config) {

    var resource = $resource(Config.base + '/schedulings/:id', { id: '@id' }, {
        'update' : { method : 'PUT' },
        'query' : { method : 'GET', isArray:false },
        'reportsStores' : { method : 'GET', isArray:false , url: Config.base + '/schedulings/reports/stores' },
        'reportsEvents' : { method : 'GET', isArray:false , url: Config.base + '/schedulings/reports/events' },
        'reportsVisits' : { method : 'GET', isArray:false , url: Config.base + '/schedulings/reports/visits' },
        'latest' : { method : 'GET', isArray:false , url: Config.base + '/schedulings/latest' },
        'feedSchedules' : { method : 'GET', isArray:false , url: Config.base + '/schedulings/feedSchedules' },
    });

    resource = TokenHandler.wrapActions( resource, ['get', 'save', 'query', 'update' , 'reportsStores', 'reportEvents', 'latest' , 'feedSchedules'] );

    return resource;

}])