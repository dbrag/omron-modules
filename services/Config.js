var environment, baseUrl;

environment = 'production';
//environment = 'development';

var local = 'http://api.omron.app/api/v1';
var dev = 'http://apidev-guiaon-dev.getup.io/api/v1';


switch(environment)
{
    case 'production':
        baseUrl = 'http://apiv2.guiaon.net/api/v1'
        break;
    case 'web':
        baseUrl = 'https://apiweb-omron.getup.io/api/v1';
        break;
    case 'development':
        local = baseUrl = dev;
        break;
    default:
        break;
}

omronModules.constant('Config', {
    base:(window.location.host == "guiaon.net" || window.location.host == "www.guiaon.net" || window.location.host == "" || window.location.host == "guiaon.net" ? baseUrl:dev),
    //base : 'http://dev-omron.getup.io/api/v1',
    hostApi:'http://api.omron.app',
    isOnlineUrl:'https://apiv2.guiaon.net',

    webVersion:"1.0.9",
    version:"1.3.0",

    schedulings:{
        startEdit:(new moment("2015-09-24")).startOf("day"),
        endEdit:(new moment("2015-09-24")).endOf("day"),
    },
    startState:"SP",
    startRegion:"Sudeste",
    tracking:{
        //fromDate: new moment().startOf("day").format(),
        fromDate:new moment().subtract(1, 'day').startOf("day").format(),
    }
});
