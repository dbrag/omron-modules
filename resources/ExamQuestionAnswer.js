omronModules.factory('ExamQuestionAnswer', [
    '$resource',
    'TokenHandler',
    'Config',
    function($resource, TokenHandler, Config)
    {
        var resource = $resource(Config.base + '/exam/question/answers/:id', {id: '@id'}, {
            'query' : {method : 'GET', isArray:false},
            'update' : { method : 'PUT' },
            'delete' : { method : 'DELETE' }
        });

        resource = TokenHandler.wrapActions(resource, ['get', 'save', 'update', 'query', 'delete']);

        return resource;
    }
]);