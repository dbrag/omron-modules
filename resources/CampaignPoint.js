omronModules.factory('CampaignPoint', [
    '$resource',
    'TokenHandler',
    'Config',
function($resource, TokenHandler, Config) {

    var resource = $resource(Config.base + '/campaignsPoints/:id', { id: '@id' }, {
        'update' : { method : 'PUT' },
        'query' : { method : 'GET', isArray:false },
        'rankingMobile' : { method : 'GET', url: Config.base + '/campaignsPointsMobile/user/:id' },
        'pointsUser' : { method : 'GET', url: Config.base + '/campaignsPoints/user/:id' }
    });

    resource = TokenHandler.wrapActions( resource, ['get', 'save', 'query', 'update', 'rankingMobile', 'pointsUser'] );

    return resource;

}])