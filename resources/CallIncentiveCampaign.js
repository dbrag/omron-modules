omronModules.factory('CallIncentiveCampaign', [
    '$resource',
    'TokenHandler',
    'Config',
function($resource, TokenHandler, Config) {

    var resource = $resource(Config.base + '/callsIncentivesCampaigns/:id', { id: '@id' }, {
        'update' : { method : 'PUT' },
        'query' : { method : 'GET', isArray:false },
        'feed' : { method : 'GET', isArray:false,  url: Config.base + '/callsIncentivesCampaigns/feed' }
    });

    resource = TokenHandler.wrapActions( resource, ['get', 'save', 'query', 'update' , 'feed'] );

    return resource;

}])