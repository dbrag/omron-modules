omronModules.factory('Network', [
    '$resource',
    'TokenHandler',
    'Config',
function($resource, TokenHandler, Config) {

    var resource = $resource(Config.base + '/networks/:id', { id: '@id' }, {
        'update' : { method : 'PUT' },
        'query' : { method : 'GET', isArray:false }
    });

    resource = TokenHandler.wrapActions( resource, ['get', 'save', 'query', 'update'] );

    return resource;

}])