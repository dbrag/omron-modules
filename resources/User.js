omronModules.factory('User', [
    '$resource',
    'TokenHandler',
    'Config',
function($resource, TokenHandler, Config) {

    var resource = $resource(Config.base + '/users/:id', { id: '@id' }, {
        'update' : { method : 'PUT' },
        'query' : { method : 'GET', isArray:false },
        'radius' : { method : 'GET', url: Config.base + '/users/:id/radius' },
        'suggests' : { method : 'GET', url: Config.base + '/users/:id/suggests' },
        'reports' : { method : 'GET', isArray:false , url: Config.base + '/users/reports' },
        'trackings' : { method : 'GET', isArray:false , url: Config.base + '/users/trackings' },
        'reportsVisits' : { method : 'GET', isArray:false , url: Config.base + '/users/reports/visits' },
        'reportsKm' : { method : 'GET', isArray:false , url: Config.base + '/users/reports/km' },
        'emailActivation' : { method : 'GET', isArray:false , url: Config.base + '/users/:id/emailActivation' },
        'emailPassword' : { method : 'GET', isArray:false , url: Config.base + '/users/:id/emailPassword' },
        'emailCorrection' : { method : 'POST', isArray:false , url: Config.base + '/users/:id/emailCorrection' },
        'notifyUser' : { method : 'POST', isArray:false , url: Config.base + '/users/:id/notifyUser' },
        'setRegId' : { method : 'PUT', isArray:false , url: Config.base + '/users/:id/setRegId' },
        'notifyCollectionUsers' : { method : 'POST', isArray:false , url: Config.base + '/users/notifyCollectionUsers' },
        'feedSchedules' : { method : 'GET', isArray:false , url: Config.base + '/users/feedSchedules' }
    });

    resource = TokenHandler.wrapActions( resource, ['get', 'save', 'query', 'update', 'radius', 'suggests' , 'reports', 'trackings', 'emailCorrection', 'notifyUser', 'setRegId', 'reportsVisits' , 'notifyCollectionUsers' , 'feedSchedules'] );

    return resource;

}])