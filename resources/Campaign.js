omronModules.factory('Campaign', [
    '$resource',
    'TokenHandler',
    'Config',
function($resource, TokenHandler, Config) {

    var resource = $resource(Config.base + '/campaigns/:id', { id: '@id' }, {
        'update' : { method : 'PUT' },
        'query' : { method : 'GET', isArray:false },
        'findByRegion' : { method : 'GET', isArray : false, url: Config.base + '/campaigns/region' },
        'current' : { method : 'GET', isArray : false, url: Config.base + '/campaigns/current' },
        'reportsMaterials' : { method : 'GET', isArray:false , url: Config.base + '/campaigns/reports/materials' },
    });

    resource = TokenHandler.wrapActions( resource, ['get', 'save', 'query', 'update', 'findByRegion', 'current', 'reportsMaterials'] );

    return resource;

}])