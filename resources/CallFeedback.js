omronModules.factory('CallFeedback', [
    '$resource',
    'TokenHandler',
    'Config',
    function($resource, TokenHandler, Config) {

        var resource = $resource(Config.base + '/callsFeedbacks/:id', { id: '@id' }, {
            'update' : { method : 'PUT' },
            'query' : { method : 'GET', isArray:false },
            'reports' : { method : 'GET', isArray:false , url: Config.base + '/callsFeedbacks/reports' },
        });

        resource = TokenHandler.wrapActions( resource, ['get', 'save', 'query', 'update' , 'reports'] );

        return resource;

    }])