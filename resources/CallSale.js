omronModules.factory('CallSale', [
    '$resource',
    'TokenHandler',
    'Config',
function($resource, TokenHandler, Config) {

    var resource = $resource(Config.base + '/callsSales/:id', { id: '@id' }, {
        'update' : { method : 'PUT' },
        'query' : { method : 'GET', isArray:false },
        'reports' : { method : 'GET', isArray:false , url: Config.base + '/sales/reports' },
        'reportsSellout' : { method : 'GET', isArray:false , url: Config.base + '/sales/reports/sellout' },
    });

    resource = TokenHandler.wrapActions( resource, ['get', 'save', 'query', 'update'] );

    return resource;

}])