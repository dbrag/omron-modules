omronModules.factory('TokenHandler', [
    '$localStorage',
    '$http',
    function($localStorage, $http)
    {
        var tokenHandler = {};

        tokenHandler.setToken = function(newToken){
            $localStorage.userToken = newToken;
        };

        tokenHandler.getToken = function(){
            return $localStorage.userToken;
        };

        tokenHandler.wrapActions = function(resource, actions){
            var wrappedResource = resource;
            for(var i = 0; i < actions.length; i++)
            {
                tokenWrapper(wrappedResource, actions[i]);
            };
            return wrappedResource;
        };

        var tokenWrapper = function(resource, action){

            resource['_' + action]  = resource[action];
            resource[action] = function(param, data, success, error)
            {
                var interceptSuccess = function(responseData, headers)
                {
                    if(headers)
                    {
                        var token = headers('X-Auth-Token');
                        if(token)tokenHandler.setToken(token);
                        if(angular.isFunction(success))success(responseData, headers);
                    }
                };

                $http.defaults.headers.common['X-Auth-Token'] = tokenHandler.getToken();

                return resource['_' + action](
                    angular.extend({}, param || {}),
                    data,
                    interceptSuccess,
                    error
                );
            };
        };

        return tokenHandler;
    }
])
