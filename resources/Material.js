omronModules.factory('Material', [
    '$resource',
    'TokenHandler',
    'Config',
function($resource, TokenHandler, Config) {

    var resource = $resource(Config.base + '/materials', {}, {
        'query' : { method : 'GET', isArray:false }
    });

    resource = TokenHandler.wrapActions( resource, ['get'] );

    return resource;

}])