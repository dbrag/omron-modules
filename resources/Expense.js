omronModules.factory('Expense', [
    '$resource',
    'TokenHandler',
    'Config',
function($resource, TokenHandler, Config) {

    var resource = $resource(Config.base + '/expenses/:id', { id: '@id' }, {
        'update' : { method : 'PUT' },
        'query' : { method : 'GET', isArray:false },
        'reports' : { method : 'GET', isArray:false, url: Config.base + '/expenses/reports' },
        'reportsImages' : { method : 'GET', isArray:false, url: Config.base + '/expenses/images' },
    });

    resource = TokenHandler.wrapActions( resource, ['get', 'save', 'query', 'update' , 'reports', 'reportsImages'] );

    return resource;

}])