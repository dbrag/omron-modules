omronModules.factory('Store', [
    '$resource',
    'TokenHandler',
    'Config',
function($resource, TokenHandler, Config) {

    var resource = $resource(Config.base + '/stores/:id', { id: '@id' }, {
        'update' : { method : 'PUT' },
        'query' : { method : 'GET', isArray:false },
        'findByLocation' : { method : 'GET', isArray:false, url: Config.base + '/stores/location' },
        'reports' : { method : 'GET', isArray:false , url: Config.base + '/stores/reports' },
        'productRuptures' : { method : 'GET', isArray:false , url: Config.base + '/stores/reports/products/ruptures' },
        'cities' : { method : 'GET', isArray:false , url: Config.base + '/stores/cities' },
    });

    resource = TokenHandler.wrapActions( resource, ['get', 'save', 'query', 'update' , 'findByLocation' , 'reports', 'productRuptures', 'cities' ] );
    return resource;

}])