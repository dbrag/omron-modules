omronModules.factory('ExamUser', [
    '$resource',
    'TokenHandler',
    'Config',
    function($resource, TokenHandler, Config)
    {
        var resource = $resource(Config.base + '/exam/users/:id', {id: '@id'}, {
            'query': {method : 'GET', isArray:false},
            'update': {method : 'PUT'},
            'start': {method : 'POST'}
        });

        resource = TokenHandler.wrapActions(resource, ['get', 'query', 'update', 'start']);

        return resource;
    }
]);