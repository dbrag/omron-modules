omronModules.factory('Notification', [
    '$resource',
    'TokenHandler',
    'Config',
function($resource, TokenHandler, Config) {

    var resource = $resource(Config.base + '/notifications', {}, {
        'query' : { method : 'GET', isArray:false }
    });

    resource = TokenHandler.wrapActions( resource, [ 'query' ] );

    return resource;

}])