omronModules.factory('Justification', [
    '$resource',
    'TokenHandler',
    'Config',
function($resource, TokenHandler, Config) {

    var resource = $resource(Config.base + '/justifications/:id', { id: '@id' }, {
        'update' : { method : 'PUT' },
        'query' : { method : 'GET', isArray:false },
        'reportJustifications' : { method : 'GET', isArray:false , url: Config.base + '/justifications/reports/visits' },
    });

    resource = TokenHandler.wrapActions( resource, ['get', 'save', 'query', 'update' , 'reportJustifications'] );

    return resource;

}])