omronModules.factory('Auth', [
    '$q',
    '$http',
    '$localStorage',
    'Config',
    '$rootScope',
    '$state',
function($q, $http, $localStorage, Config,$rootScope,$state) {
    var auth = {};
    //$rootScope.$on('unauthorized', auth.forceLogout )
    auth.authenticate = function(credentials) {

        return $http.post(Config.base + '/auth', credentials ).then(function(response) {
            $localStorage.userId = response.data.id;
            $localStorage.userToken = response.data.api_token;
            $localStorage.userData = {
                id: response.data.id,
                role: response.data.role,
                name: response.data.name,
                email: response.data.email,
                should_update_schedule: response.data.should_update_schedule,
                schedule_updated_at : response.data.schedule_updated_at,
                groups: response.data.groups,
                avatar: (response.data.profile ? response.data.profile.avatar : null)
            }
            return response;
        });
    };

    auth.logout = function() {
        //@todo: put this on token handler?
        $http.defaults.headers.common['X-Auth-Token'] = $localStorage.userToken;
        $localStorage.$reset();//reset always - since api return error;
        return $http.get(Config.base + '/logout' ).then(function(response) {
            return response.data;
        });
    };

    auth.forceLogout = function () {
        var q = $q.defer();
        q.resolve($localStorage.$reset());
        return q.promise;
    }

    auth.isAuthenticated = function() {
        return !$localStorage.userId || !$localStorage.userToken;
    };

    auth.isLogged = function() {
        return $localStorage.userId && $localStorage.userToken;
    };

    auth.isAuthorized = function(authorizedRoles) {
        if (!angular.isArray(authorizedRoles)) {
            authorizedRoles = [authorizedRoles];
        }
        return (auth.isAuthenticated() && authorizedRoles.indexOf($localStorage.UserRoles) !== -1);
    };

    auth.activation = function(data) {
        return $http.post(Config.base + '/auth/activation/' + data.credentials.email + '/' + data.code , data ).then(function(response) {
            $localStorage.userId = response.data.id;
            $localStorage.userToken = response.data.api_token;
            $localStorage.userData = {
                id: response.data.id,
                role: response.data.role,
                name: response.data.name,
                email: response.data.email,
                should_update_schedule: response.data.should_update_schedule,
                schedule_updated_at : response.data.schedule_updated_at,
                groups: response.data.groups,
                avatar: (response.data.profile ? response.data.profile.avatar : null)
            }
            return response;
        });
    };

    auth.forget = function(data) {
        return $http.post(Config.base + '/auth/forget/', data ).then(function(response) {
            return response;
        });
    };

    auth.reset = function(data) {
        return $http.post(Config.base + '/auth/reset-password/' + data.credentials.email + '/' + data.code , data.credentials ).then(function(response) {
            $localStorage.userId = response.data.id;
            $localStorage.userToken = response.data.api_token;
            $localStorage.userData = {
                id: response.data.id,
                role: response.data.role,
                name: response.data.name,
                email: response.data.email,
                should_update_schedule: response.data.should_update_schedule,
                schedule_updated_at : response.data.schedule_updated_at,
                groups: response.data.groups,
                avatar: (response.data.profile ? response.data.profile.avatar : null)
            }
            return response;
        });
    };

    return auth;
}])