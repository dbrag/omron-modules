omronModules.factory('Product', [
    '$resource',
    'TokenHandler',
    'Config',
function($resource, TokenHandler, Config) {

    var resource = $resource(Config.base + '/products/:id', { id: '@id' }, {
        'update' : { method : 'PUT' },
        'query' : { method : 'GET', isArray:false },
        'reports' : { method : 'GET', isArray:false , url: Config.base + '/products/reports' },
        'variation' : { method : 'GET', isArray:false , url: Config.base + '/products/reportsByProduct' },
        'incidents' : { method : 'GET', isArray:false , url: Config.base + '/products/reports/incidents' },
        'reportsExhibitions' : { method : 'GET', isArray: false , url: Config.base + '/products/reports/exhibitions' },
        'reportsTypesExhibitions' : { method : 'GET', isArray:false , url: Config.base + '/products/reports/types' },
        'reportsImages' : { method : 'GET', isArray:false , url: Config.base + '/products/reports/exhibitions/images' },
        'reportsExhibitionsImages' : { method : 'GET', isArray:false , url: Config.base + '/products/exhibitions/images' },
    });

    resource = TokenHandler.wrapActions( resource, ['get', 'save', 'query', 'update' , 'reports','variation',
        'reportsExhibitions' ,'incidents', 'reportsTypesExhibitions', 'reportsImages' ,'reportsExhibitionsImages'] );

    return resource;

}])