omronModules.factory('Brand', [
    '$resource',
    'TokenHandler',
    'Config',
function($resource, TokenHandler, Config) {

    var resource = $resource(Config.base + '/brands/:id', { id: '@id' }, {
        'update' : { method : 'PUT' },
        'query' : { method : 'GET', isArray:false }
    });

    resource = TokenHandler.wrapActions( resource, ['get', 'save', 'query', 'update'] );

    return resource;

}])