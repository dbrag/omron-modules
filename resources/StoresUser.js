omronModules.factory('StoresUser', [
    '$resource',
    'TokenHandler',
    'Config',
function($resource, TokenHandler, Config) {

    var resource = $resource(Config.base + '/storesUsers/:id', { id: '@id' }, {
        'update' : { method : 'PUT' },
        'query' : { method : 'GET', isArray:false },
        'clearAllStores' : { method : 'DELETE', isArray:false , url: Config.base + '/storesUsers/clearAllStores' },
    });

    resource = TokenHandler.wrapActions( resource, ['get', 'save', 'query', 'update' ] );
    return resource;

}])