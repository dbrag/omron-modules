omronModules.factory('Workday', [
    '$resource',
    'TokenHandler',
    'Config',
function($resource, TokenHandler, Config) {

    var resource = $resource(Config.base + '/workdays/:id', { id: '@id' }, {
        'update' : { method : 'PUT' },
        'query' : { method : 'GET', isArray:false },
        'findByDay' : { method : 'GET', isArray:false, url: Config.base + '/workdays/current/:id' },
        'lastDay' : { method : 'GET', isArray:false, url: Config.base + '/workdays/last/:id' },
        'reportsKm' : { method : 'GET', isArray:false, url: Config.base + '/workdays/reports' },
        'reportsKmImages' : { method : 'GET', isArray:false, url: Config.base + '/workdays/km/images' },
        'reportsEndOfDay' : { method : 'GET', isArray:false, url: Config.base + '/workdays/reportsEndOfDay' },
        'feedWorkdays' : { method : 'GET', isArray:false , url: Config.base + '/workdays/feedWorkdays' }
    });

    resource = TokenHandler.wrapActions( resource, ['get', 'save', 'query', 'update', 'findByDay', 'lastDay', 'reportsKm', 'reportsKmImages', 'reportsEndOfDay' , 'feedWorkdays'] );

    return resource;

}])